from setuptools import setup

setup(
    name='evaluation_report',
    version='',
    packages=['test', 'evaluation_report'],
    url='',
    license='',
    author='sovanne',
    author_email='',
    description='',
    install_requires=[
        'pymongo',
        "straikit @ git+ssh://git@bitbucket.org/stradigi/straikit.git",
        'moto', 'boto3', 'numpy'
    ]
)

import io
import random

import boto3
import numpy as np
import mongomock
import pymongo
from PIL import Image
from moto import mock_s3

from evaluation_report.main import generate_report


def insert_random_data(my_collection, s3_connection, tmp_path):
    for i in range(500):
        # randomly generate images and upload it to s3, keep file path in DB
        mask = np.random.randint(0, 3, (32, 32, 3))
        image = np.random.randint(0, 3, (32, 32, 3))

        pil_image = Image.fromarray(image.astype('uint8'), mode='RGB')
        pil_mask = Image.fromarray(mask.astype('uint8'), mode='RGB')

        pil_image.save('%s/image_%d.png' % (tmp_path, i), 'PNG')
        pil_mask.save('%s/mask_%d.png' % (tmp_path, i), 'PNG')

        s3_connection.upload_file('%s/mask_%d.png' % (tmp_path, i),
                              'inference',
                              '/pipeline0/mask_%d.png' % i)

        s3_connection.upload_file('%s/image_%d.png' % (tmp_path, i),
                              'inference',
                              '/pipeline0/image_%d.png' % i)

        example = {'body_mask': '/inference/pipeline0/mask_%d.png' % i,
                   'body_image': '/inference/pipeline0/image_%d.png' % i,
                   'body_pipelineId': '5d9e1e46dfa1a0000f207a1e'}

        # tabular data (multiple dimension)
        for j in range(4):
            example['pred_y_%d' % j] = random.randint(0, 9)
            example['pred_y_%d_duplicate' % j] = example['pred_y_%d' % j]
            example['true_y_%d' % j] = random.randint(0, 9)
            example['true_y_%d_duplicate' % j] = example['true_y_%d' % j]

        # classification label
        example['pred_y_str'] = ['dog' if random.randint(0, 9) % 2 == 0
                                 else 'cat']
        example['true_y_str'] = ['dog' if random.randint(0, 9) % 2 == 0
                                 else 'cat']
        example['pred_y_str_duplicate'] = example['pred_y_str']

        my_collection.insert(example)


@mock_s3
@mongomock.patch(servers=(('127.0.0.1', 27017),))
def test_with_image(tmp_path):
    """
    This test is a bit long because it combine all the test together to reduce
    the IO time.
    Parameters
    ----------
    tmp_path :
    """
    myclient = pymongo.MongoClient('mongodb://127.0.0.1:27017')
    mydb = myclient["sentiment-analysis"]
    mycol = mydb["churn"]

    s3_connection = boto3.resource('s3', region_name='us-east-1')
    s3_connection.create_bucket(Bucket='inference')

    s3_client = boto3.client('s3', region_name='us-east-1')

    insert_random_data(mycol, s3_client, tmp_path)

    # test metrics with images
    metrics = generate_report(
        url='mongodb://127.0.0.1:27017',
        database_name='sentiment-analysis',
        collection_name='churn',
        y_true_columns=['body_mask'],
        y_pred_columns=['body_image'],
        metrics=['classification_report', 'confusion_matrix', 'accuracy'],
        pipeline_id='5d9e1e46dfa1a0000f207a1e',
        as_json=False
    )

    # random data will produce accuracy of 0.33 with 3 classes
    assert abs(metrics['accuracy'] - 0.33) <= 1e-2
    assert 'confusion_matrix' in metrics.keys()

    # test interface metrics with tabular data
    metrics = generate_report(
        url='mongodb://127.0.0.1:27017',
        database_name='sentiment-analysis',
        collection_name='churn',
        y_true_columns=['pred_y_%d' % i for i in range(4)],
        y_pred_columns=['true_y_%d' % i for i in range(4)],
        metrics=['mse', 'rmse', 'mae', 'r2'],
        pipeline_id='5d9e1e46dfa1a0000f207a1e',
        as_json=False
    )

    # test calculation
    metrics = generate_report(
        url='mongodb://127.0.0.1:27017',
        database_name='sentiment-analysis',
        collection_name='churn',
        y_true_columns=['pred_y_%d' % i for i in range(4)],
        y_pred_columns=['pred_y_%d_duplicate' % i for i in range(4)],
        metrics=['mse', 'rmse', 'mae', 'r2'],
        pipeline_id='5d9e1e46dfa1a0000f207a1e',
        as_json=False
    )
    assert metrics['mse'] == 0.
    assert metrics['rmse'] == 0.
    assert metrics['mae'] == 0.
    assert metrics['r2'] == 1.

    # test string
    metrics = generate_report(
        url='mongodb://127.0.0.1:27017',
        database_name='sentiment-analysis',
        collection_name='churn',
        y_true_columns=['true_y_str'],
        y_pred_columns=['pred_y_str'],
        metrics=['accuracy', 'confusion_matrix', 'classification_report'],
        pipeline_id='5d9e1e46dfa1a0000f207a1e',
        as_json=False
    )
    assert abs(metrics['accuracy'] - 0.5) < 1e-1
    assert 'value' in metrics['confusion_matrix'].keys()
    assert len(metrics['confusion_matrix']['label']) == 2 \
           and 'dog' in metrics['confusion_matrix']['label'] \
           and 'cat' in metrics['confusion_matrix']['label']

@mock_s3
def test_encoding(tmp_path):

    s3_connection = boto3.resource('s3', region_name='us-east-1')
    s3_connection.create_bucket(Bucket='inference')

    s3_client = boto3.client('s3', region_name='us-east-1')
    image = np.random.randint(0, 3, (4, 4, 3), 'uint8')

    pil_image = Image.fromarray(image.astype('uint8'), mode='RGB')
    pil_image.save('%s/mask_0.png' % tmp_path, 'PNG')

    s3_client.upload_file('%s/mask_0.png' % tmp_path,
                          'inference',
                          '/pipeline0/mask_0.png')

    # download back
    file_obj = s3_connection.Object(
        bucket_name='inference',
        key='pipeline0/mask_0.png',
    )

    s3_image = Image.open(io.BytesIO(file_obj.get()['Body'].read()))
    s3_image = np.array(s3_image)
    assert (s3_image - image).sum() == 0.


@mock_s3
def test_other_image_format(tmp_path):

    s3_connection = boto3.resource('s3', region_name='us-east-1')
    s3_connection.create_bucket(Bucket='inference')

    s3_client = boto3.client('s3', region_name='us-east-1')
    image = np.random.randint(0, 3, (4, 4, 3), 'uint8')

    pil_image = Image.fromarray(image.astype('uint8'), mode='RGB')
    pil_image.save('%s/mask_0.png' % tmp_path, 'PNG')

    s3_client.upload_file('%s/mask_0.png' % tmp_path,
                          'inference',
                          '/pipeline0/mask_0.png')

    # download back
    file_obj = s3_connection.Object(
        bucket_name='inference',
        key='pipeline0/mask_0.png',
    )

    s3_image = Image.open(io.BytesIO(file_obj.get()['Body'].read()))
    s3_image = np.array(s3_image)
    assert (s3_image - image).sum() == 0.
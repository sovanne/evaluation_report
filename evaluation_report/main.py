from typing import List

from evaluation_report.connector import MongoConnector
from straikit.eval.metrics.evaluation_report import EvaluationReport


def generate_report(
        url: str,
        database_name: str,
        collection_name: str,
        y_true_columns: List[str],
        y_pred_columns: List[str],
        metrics: List[str],
        as_json: bool,
        pipeline_id):
    """

    Parameters
    ----------
    url : contain host, port and possibly username and password
    database_name : name of the database
    collection_name : name of the collection
    y_true_columns : list of columns representing y_true
    y_pred_columns : list of columns representing y_pred
    metrics : list of metrics to generate report. Possible values are: 'mse',
                'rmse', 'mae', 'confusion_matrix', 'classification_report',
                'accuracy'
    pipeline_id : id of the pipeline
    as_json : whether to return results as dictionary or json format

    Returns
    -------

    results : dict whose key are the metrics and values are the metrics. For
            confusion matrix, the label of the classes can be access by
            results['label']
             or json format depending on the input argument as_json

    """
    connector = MongoConnector(
        url=url,
        database_name=database_name,
        collection_name=collection_name,
        y_true_columns=y_true_columns,
        y_pred_columns=y_pred_columns
    )

    # get y_true, y_pred
    y_true, y_pred = connector.query(pipeline_id)

    report = EvaluationReport(metrics=metrics)
    results = report.compute(y_pred, y_true, as_json=as_json)
    return results

